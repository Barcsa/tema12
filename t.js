var element = new Vue({
  el: '#tema',
  data: {
    randomSpots: [],
    availableColors: [],
    elementsThatHaveBeenTried: [],
  },
  methods: {
    modify: function (elementTomodify, color) {
      elementTomodify.style.backgroundColor = color;
    },
    changeVisibility: function (element) {
      element.style.visibility = 'hidden';
    },
    getColor: function () {
      var color = '';
      for (var i = 0; i < 3; i++) {
        var sub = Math.floor(Math.random() * 256).toString(16);
        color += sub.length == 1 ? '0' + sub : sub;
      }
      return '#' + color;
    },
    verify: function (element) {
      this.elementsThatHaveBeenTried.push(element);
      document.getElementById(element).style.backgroundColor = this.availableColors[
        element
      ];
      this.tries = this.tries + 1;

      if (this.elementsThatHaveBeenTried.length == 2) {
        console.log(
          document.getElementById(this.elementsThatHaveBeenTried[0]).style.backgroundColor,
          document.getElementById(this.elementsThatHaveBeenTried[1]).style.backgroundColor
        );
        if (
          document.getElementById(this.elementsThatHaveBeenTried[0]).style.backgroundColor !=
            'rgb(255,255,255)' &&
          document.getElementById(this.elementsThatHaveBeenTried[1]).style.backgroundColor !=
            'rgb(255,255,255)'
        ) {
          console.log(
            document.getElementById(this.elementsThatHaveBeenTried[0]).style.backgroundColor +
              'evectiv'
          );
          setTimeout(() => {
            if (
              document.getElementById(this.elementsThatHaveBeenTried[0]).style
                .backgroundColor !=
              document.getElementById(this.elementsThatHaveBeenTried[1]).style.backgroundColor
            ) {
              this.modify(document.getElementById(this.elementsThatHaveBeenTried[0]), 'black');
              this.modify(document.getElementById(this.elementsThatHaveBeenTried[1]), 'black');
            } else {
              this.modify(
                document.getElementById(this.elementsThatHaveBeenTried[0]),
                'rgb(255,255,255)'
              );
              this.modify(
                document.getElementById(this.elementsThatHaveBeenTried[1]),
                'rgb(255,255,255)'
              );
            }
            this.reinitializearray();
          }, 1000);
        }
      }
    },
    reinitializearray: function(){
      this.elementsThatHaveBeenTried = [];
    },
    mix:  (array) => {
      array.sort(() => Math.random() - 0.5);
    },
    create: function () {
      for(let q = 0 ; q <= 15; q++)
        this.randomSpots.push(q);

      for (let i = 0; i < 8; i++) {
        for (let j = 2; j <=2; j++) {
          genColor = this.getColor();
          this.availableColors.push(genColor);
          this.availableColors.push(genColor);
          console.log("pushed " , this.availableColors)
        }
      }
      this.mix(this.randomSpots);
    },
  },
});
